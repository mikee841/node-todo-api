//const MongoClient = require('mongodb').MongoClient;
const {MongoClient, ObjectID} = require('mongodb');

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, db) => {
    if (err) {
       return console.log('Unable to connect to MongoDB server');
    }
    console.log('Connected to MongoDB server');

    //Delete many
    /*db.collection('Todos').deleteMany({text: 'Eat lunch'}).then((result) => {
        console.log(result);
    });*/

    /*db.collection('Todos').deleteOne({text: 'Eat lunch'}).then((result) => {
        console.log(result);
    });*/

    db.collection('Users').deleteMany({name: 'Miika'}).then((result) => {
        console.log(result);
    });

    db.collection('Users').findOneAndDelete({_id: ObjectID("5c10048940d0874558d8e707")}).then((result) => {
        console.log(result);
    });

    //db.close();
});