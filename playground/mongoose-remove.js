const {ObjectID} = require('mongodb');
const {mongoose} = require('../server/db/mongoose');
const {Todo} = require('../server/models/todo');
const {User} = require('../server/models/user');

/*Todo.remove({}).then((result) => {
    console.log(result);
});*/

Todo.findOneAndRemove({_id: '5c27a7c0e9a2a73218da0c0b'}).then((todo) => {
    console.log(todo);
});

Todo.findByIdAndRemove('5c27a7c0e9a2a73218da0c0b').then((todo) => {
    console.log(todo);
});