//const MongoClient = require('mongodb').MongoClient;
const {MongoClient, ObjectID} = require('mongodb');

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, db) => {
    if (err) {
       return console.log('Unable to connect to MongoDB server');
    }
    console.log('Connected to MongoDB server');

    db.collection('Todos').findOneAndUpdate(
        {
            _id: new ObjectID("5c100d47e9a2a7527888f8c1")
        },
        {
            $set: {
                text: 'Nerf'
            }
        },
        {
            returnOriginal: false
        }
        ).then( (result) => {
            console.log(result);
    });

    db.collection('Users').findOneAndUpdate({
        _id: new ObjectID("5c100877ab2a1e2828225979")
    },
    {
        $inc:
        {
            age: +1
        },
        $set: 
        {
            name: 'Miika'
        }
    },
    {
        returnOriginal: false
    }
    ).then((result) => {
        console.log(result);
    });

    //db.close();
});